import sqlite3

import TelemetryServer.CREDENTIALS as creds

####### SQL DB #######

def db_connection(db_file):
    conn = None
    try:
        conn = sqlite3.connect(db_file)
        print("SQL DB Connection established")
    except Error as e:
        print("SQL DB Connection failed", e)
    return conn

def create_tables(conn, session_name):
    sql_create_lapdata_table = f"""CREATE TABLE IF NOT EXISTS {session_name}_lapdata (
                                        id integer PRIMARY KEY,
                                        sessionTime real NOT NULL,
                                        lastLapTime real,
                                        currentLapTime real,
                                        lapDistance real,
                                        totalDistance real,
                                        carPosition integer,
                                        currentLapNum integer);"""

    sql_create_telemetry_table = f"""CREATE TABLE IF NOT EXISTS {session_name}_telemetry (
                                        id integer PRIMARY KEY,
                                        sessionTime real NOT NULL,
                                        speed integer,
                                        throttle real,
                                        steer real,
                                        brake real,
                                        gear integer,
                                        engineRPM real,
                                        drs integer,
                                        brakesTemperature_RL real,
                                        brakesTemperature_RR real,
                                        brakesTemperature_FL real,
                                        brakesTemperature_FR real,
                                        tyresSurfaceTemperature_RL real,
                                        tyresSurfaceTemperature_RR real,
                                        tyresSurfaceTemperature_FL real,
                                        tyresSurfaceTemperature_FR real,
                                        tyresInnerTemperature_RL real,
                                        tyresInnerTemperature_RR real,
                                        tyresInnerTemperature_FL real,
                                        tyresInnerTemperature_FR real,
                                        engineTemperature real);"""

    try:
        c = conn.cursor()
        c.execute(sql_create_lapdata_table)
        c.execute(sql_create_telemetry_table)
    except Error as e:
        print(e)

def write_lap_data_to_sql(conn, sessionTime, car_laptime_data, session_name):
    """
    writes the Lapdata to a SQL Database
    """
    sql = f'''INSERT INTO {session_name}_lapdata (sessionTime, lastLapTime, currentLapTime, lapDistance, totalDistance, carPosition, currentLapNum) VALUES(?,?,?,?,?,?,?)'''
    data = (
        sessionTime,
        car_laptime_data.lastLapTime,
        car_laptime_data.currentLapTime,
        car_laptime_data.lapDistance,
        car_laptime_data.totalDistance,
        car_laptime_data.carPosition,
        car_laptime_data.currentLapNum
    )
    cur = conn.cursor()
    cur.execute(sql, data)
    conn.commit()

def write_telemetry_data_to_sql(conn, sessionTime, car_telemetry_data, session_name):
    """
    writes the telemetry data to a SQL Database
    """
    data = (
        sessionTime,
        car_telemetry_data.speed,
        car_telemetry_data.throttle,
        car_telemetry_data.steer,
        car_telemetry_data.brake,
        car_telemetry_data.gear,
        car_telemetry_data.engineRPM,
        car_telemetry_data.drs,
        car_telemetry_data.brakesTemperature[0],
        car_telemetry_data.brakesTemperature[1],
        car_telemetry_data.brakesTemperature[2],
        car_telemetry_data.brakesTemperature[3],
        car_telemetry_data.tyresSurfaceTemperature[0],
        car_telemetry_data.tyresSurfaceTemperature[1],
        car_telemetry_data.tyresSurfaceTemperature[2],
        car_telemetry_data.tyresSurfaceTemperature[3],
        car_telemetry_data.tyresInnerTemperature[0],
        car_telemetry_data.tyresInnerTemperature[1],
        car_telemetry_data.tyresInnerTemperature[2],
        car_telemetry_data.tyresInnerTemperature[3],
        car_telemetry_data.engineTemperature
    )

    sql = f'''INSERT INTO {session_name}_telemetry (
        sessionTime, 
        speed, 
        throttle, 
        steer, 
        brake, 
        gear, 
        engineRPM, 
        drs, 
        brakesTemperature_RL, 
        brakesTemperature_RR,
        brakesTemperature_FL,
        brakesTemperature_FR,
        tyresSurfaceTemperature_RL,
        tyresSurfaceTemperature_RR,
        tyresSurfaceTemperature_FL,
        tyresSurfaceTemperature_FR,
        tyresInnerTemperature_RL,
        tyresInnerTemperature_RR,
        tyresInnerTemperature_FL,
        tyresInnerTemperature_FR,
        engineTemperature) VALUES({'?' + (len(data)-1)*',?'})'''
    
    cur = conn.cursor()
    cur.execute(sql, data)
    conn.commit()

####### INFLUX DB #######

from datetime import datetime
from influxdb_client import InfluxDBClient, Point, WritePrecision
from influxdb_client.client.write_api import SYNCHRONOUS

def influx_connection():
    try:
        conn = InfluxDBClient(url=creds.INFLUXDB_HOST, token=creds.INFLUXDB_TOKEN)
        print("Influx Connection established")
    except Error as e:
        print("Influx Connection Failed", e)
        conn = None
    return conn

def write_telemetry_data_to_influx(client, car_telemetry_data):
    data = {
        "speed": car_telemetry_data.speed,
        "throttle": car_telemetry_data.throttle,
        "steer": car_telemetry_data.steer,
        "brake": car_telemetry_data.brake,
        "gear": car_telemetry_data.gear,
        "engineRPM": car_telemetry_data.engineRPM,
        "drs": car_telemetry_data.drs,
        "brakesTemperature_RL": car_telemetry_data.brakesTemperature[0],
        "brakesTemperature_RR": car_telemetry_data.brakesTemperature[1],
        "brakesTemperature_FL": car_telemetry_data.brakesTemperature[2],
        "brakesTemperature_FR": car_telemetry_data.brakesTemperature[3],
        "tyresSurfaceTemperature_RL": car_telemetry_data.tyresSurfaceTemperature[0],
        "tyresSurfaceTemperature_RR": car_telemetry_data.tyresSurfaceTemperature[1],
        "tyresSurfaceTemperature_FL": car_telemetry_data.tyresSurfaceTemperature[2],
        "tyresSurfaceTemperature_FR": car_telemetry_data.tyresSurfaceTemperature[3],
        "tyresInnerTemperature_RL": car_telemetry_data.tyresInnerTemperature[0],
        "tyresInnerTemperature_RR": car_telemetry_data.tyresInnerTemperature[1],
        "tyresInnerTemperature_FL": car_telemetry_data.tyresInnerTemperature[2],
        "tyresInnerTemperature_FR": car_telemetry_data.tyresInnerTemperature[3],
        "engineTemperature": car_telemetry_data.engineTemperature
    }

    measurement = creds.INFLUXDB_MEASUREMENT
    tag = 'packet_type'
    tag_value = 'telemetry'
    sequence = []
    for point in data:
        string = f"{measurement},{tag}={tag_value} {point}={data[point]}"
        sequence.append(string)
    
    write_api = client.write_api(write_options=SYNCHRONOUS).write(creds.INFLUXDB_BUCKET, creds.INFLUXDB_ORG, sequence)
    print('telemetry data successfully written to influxdb')

def write_lap_data_to_influx(client, car_laptime_data):
    data = {
        "lastLapTime": car_laptime_data.lastLapTime,
        "currentLapTime": car_laptime_data.currentLapTime,
        "lapDistance": car_laptime_data.lapDistance,
        "totalDistance": car_laptime_data.totalDistance,
        "carPosition": car_laptime_data.carPosition,
        "currentLapNum": car_laptime_data.currentLapNum
    }

    measurement = creds.INFLUXDB_MEASUREMENT
    tag = 'packet_type'
    tag_value = 'lapdata'
    sequence = []
    for point in data:
        string = f"{measurement},{tag}={tag_value} {point}={data[point]}"
        sequence.append(string)
    
    write_api = client.write_api(write_options=SYNCHRONOUS)
    write_api.write(creds.INFLUXDB_BUCKET, creds.INFLUXDB_ORG, sequence)
    print('lapdata successfully written to influxdb')

def write_session_data_to_influx(client, session_data):
    data = {
        "trackTemperature": session_data.trackTemperature,
        "airTemperature": session_data.airTemperature,
        "totalLaps": session_data.totalLaps,
        "trackLength": session_data.trackLength,
        "sessionType": session_data.sessionType # 0 = unknown, 1 = P1, 2 = P2, 3 = P3, 4 = Short P5 = Q1, 6 = Q2, 7 = Q3, 8 = Short Q, 9 = OSQ 10 = R, 11 = R2, 12 = Time Trial
    }

    measurement = creds.INFLUXDB_MEASUREMENT
    tag = 'packet_type'
    tag_value = 'session'
    sequence = []
    for point in data:
        string = f"{measurement},{tag}={tag_value} {point}={data[point]}"
        sequence.append(string)
    
    write_api = client.write_api(write_options=SYNCHRONOUS)
    write_api.write(creds.INFLUXDB_BUCKET, creds.INFLUXDB_ORG, sequence)
    print('session data successfully written to influxdb')

def write_car_status_data_to_influx(client, car_status_data):
    data = {
        "fuelMix": car_status_data.fuelMix,
        "fuelRemainingLaps": car_status_data.fuelRemainingLaps,
        "tyresWear_RL": car_status_data.tyresWear[0],
        "tyresWear_RR": car_status_data.tyresWear[1],
        "tyresWear_FL": car_status_data.tyresWear[2],
        "tyresWear_FR": car_status_data.tyresWear[3],
        "visualTyreCompound": car_status_data.visualTyreCompound, # 16 = soft, 17 = medium, 18 = hard, 7 = inter, 8 = wet
        "tyresAgeLaps": car_status_data.tyresAgeLaps,
        "ersStoreEnergy": car_status_data.ersStoreEnergy
    }

    measurement = creds.INFLUXDB_MEASUREMENT
    tag = 'packet_type'
    tag_value = 'carstatus'
    sequence = []
    for point in data:
        string = f"{measurement},{tag}={tag_value} {point}={data[point]}"
        sequence.append(string)
    
    write_api = client.write_api(write_options=SYNCHRONOUS)
    write_api.write(creds.INFLUXDB_BUCKET, creds.INFLUXDB_ORG, sequence)
    print('carstatus data successfully written to influxdb')