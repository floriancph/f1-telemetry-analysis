import socket
from f1_2020_telemetry.packets import unpack_udp_packet
import time
import os

import TelemetryServer.DatabaseHandler as dbh
import TelemetryServer.CREDENTIALS as creds

def start_session(session_name):
    print(f"Session {session_name} started...")
    # Create/Configure Databases and connect to it
    conn = dbh.db_connection(creds.SQL_DATABASE_PATH)
    dbh.create_tables(conn, session_name)
    conn_influx = dbh.influx_connection()
    
    udp_socket = socket.socket(family=socket.AF_INET, type=socket.SOCK_DGRAM)
    udp_socket.bind(("", creds.UDP_PORT))
    print(f"Listening to Port {creds.UDP_PORT}...")

    # Receive Packages
    while True:
        udp_packet = udp_socket.recv(2048)
        packet = unpack_udp_packet(udp_packet)

        packet_type = packet.header.packetId # get the packet type from the header
        position = packet.header.playerCarIndex # get the position of the driver in the list as the packet contains data of all driving cars

        # SESSION DATA
        if packet_type == 1:
            dbh.write_session_data_to_influx(conn_influx, packet)
        
        # LAP DATA
        if packet_type == 2:
            car_laptime_data = packet.lapData[position]
            dbh.write_lap_data_to_influx(conn_influx, car_laptime_data)
            dbh.write_lap_data_to_sql(conn, packet.header.sessionTime, car_laptime_data, session_name)

        # TELEMETRY DATA
        if packet_type == 6:
            car_telemetry_data = packet.carTelemetryData[position]
            dbh.write_telemetry_data_to_influx(conn_influx, car_telemetry_data)
            dbh.write_telemetry_data_to_sql(conn, packet.header.sessionTime, car_telemetry_data, session_name)

        # CAR STATUS DATA
        if packet_type == 7:
            car_status_data = packet.carStatusData[position]
            dbh.write_car_status_data_to_influx(conn_influx, car_status_data)