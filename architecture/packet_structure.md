# 1. Header
| Datatype                   | Variable                   | Comment                                                       |
|----------------------------|----------------------------|---------------------------------------------------------------|
| uint16                     | packetFormat               | // 2020                                                       |
| uint8                      | gameMajorVersion           | // Game major version - "X.00"                                |
| uint8                      | gameMinorVersion           | // Game minor version - "1.XX"                                |
| uint8                      | packetVersion              | // Version of this packet type, all start from 1              |
| uint8                      | packetId                   | // Identifier for the packet type, see below                  |
| uint64                     | sessionUID                 | // Unique identifier for the session                          |
| float                      | sessionTime                | // Session timestamp                                          |
| uint32                     | frameIdentifier            | // Identifier for the frame the data was retrieved on         |
| uint8                      | playerCarIndex             | // Index of player's car in the array                         |
| // ADDED IN BETA 2:        |                            |                                                               |
| uint8                      | m_secondaryPlayerCarIndex  | // Index of secondary player's car in the array (splitscreen) |
| // 255 if no second player |                            |                                                               |

## 1.1 -> PacketId
| Packet Name          | Value | Description                                                                      |
|----------------------|-------|----------------------------------------------------------------------------------|
| Motion               |   0   | Contains all motion data for player’s car – only sent while player is in control |
| Session              |   1   | Data about the session – track, time left                                        |
| Lap Data             |   2   | Data about all the lap times of cars in the session                              |
| Event                |   3   | Various notable events that happen during a session                              |
| Participants         |   4   | List of participants in the session, mostly relevant for multiplayer             |
| Car Setups           |   5   | Packet detailing car setups for cars in the race                                 |
| Car Telemetry        |   6   | Telemetry data for all cars                                                      |
| Car Status           |   7   | Status data for all cars such as damage                                          |
| Final Classification |   8   | Final classification confirmation at the end of a race                           |
| Lobby Info           |   9   | Information about players in a multiplayer lobby                                 |

# 2. Motion Packet
| Datatype      | Variable          | Comment                       |
|---------------|-------------------|-------------------------------|
| PacketHeader  | header            | // Header                     |
| CarMotionData | carMotionData[22] | // Data for all cars on track |

& (for the player car ONLY data)

| Datatype | Variable                     | Comment                                             |
|----------|------------------------------|-----------------------------------------------------|
| float    | suspensionPosition[4]        | // Note: All wheel arrays have the following order: |
| float    | suspensionVelocity[4]        | // RL, RR, FL, FR                                   |
| float    | suspensionAcceleration[4]    | // RL, RR, FL, FR                                   |
| float    | wheelSpeed[4]                | // Speed of each wheel                              |
| float    | wheelSlip[4]                 | // Slip ratio for each wheel                        |
| float    | localVelocityX               | // Velocity in local space                          |
| float    | localVelocityY               | // Velocity in local space                          |
| float    | localVelocityZ               | // Velocity in local space                          |
| float    | angularVelocityX             | // Angular velocity x-component                     |
| float    | angularVelocityY             | // Angular velocity y-component                     |
| float    | angularVelocityZ             | // Angular velocity z-component                     |
| float    | angularAccelerationX         | // Angular velocity x-component                     |
| float    | angularAccelerationY         | // Angular velocity y-component                     |
| float    | angularAccelerationZ         | // Angular velocity z-component                     |
| float    | frontWheelsAngle             | // Current front wheels angle in radians            |

## 2.1 -> CarMotionData (for each car)
| Datatype | Variable           | Comment                                         |
|----------|--------------------|-------------------------------------------------|
| float    | worldPositionX     | // World space X position                       |
| float    | worldPositionY     | // World space Y position                       |
| float    | worldPositionZ     | // World space Z position                       |
| float    | worldVelocityX     | // Velocity in world space X                    |
| float    | worldVelocityY     | // Velocity in world space Y                    |
| float    | worldVelocityZ     | // Velocity in world space Z                    |
| int16    | worldForwardDirX   | // World space forward X direction (normalised) |
| int16    | worldForwardDirY   | // World space forward Y direction (normalised) |
| int16    | worldForwardDirZ   | // World space forward Z direction (normalised) |
| int16    | worldRightDirX     | // World space right X direction (normalised)   |
| int16    | worldRightDirY     | // World space right Y direction (normalised)   |
| int16    | worldRightDirZ     | // World space right Z direction (normalised)   |
| float    | gForceLateral      | // Lateral G-Force component                    |
| float    | gForceLongitudinal | // Longitudinal G-Force component               |
| float    | gForceVertical     | // Vertical G-Force component                   |
| float    | yaw                | // Yaw angle in radians                         |
| float    | pitch              | // Pitch angle in radians                       |
| float    | roll               | // Roll angle in radians                        |

# 3. LapData Packet
| Datatype     | Variable       | Comment                           |
|--------------|----------------|-----------------------------------|
| PacketHeader | header         | // Header                         |
| LapData      | lapData[22]    | // Lap data for all cars on track |

## 3.1 -> LapData (for each car)
| Datatype             | Variable            | Comment       |
|----------------------|---------------------|---------------|
| float                | lastLapTime         | // Last lap time in seconds |
| float                | currentLapTime      | // Current time around the lap in seconds    |
| //UPDATED in Beta 3: |       |     |
| uint16               | sector1TimeInMS      | // Sector 1 time in milliseconds         |
| uint16               | sector2TimeInMS     | // Sector 2 time in milliseconds        |
| float                | bestLapTime     | // Best lap time of the session in seconds        |
| uint8                | bestLapNum  | // Lap number best time achieved on      |
| uint16               | bestLapSector1TimeInMS   | // Sector 1 time of best lap in the session in milliseconds    |
| uint16               | bestLapSector2TimeInMS | // Sector 2 time of best lap in the session in milliseconds   |
| uint16               | bestLapSector3TimeInMS   | // Sector 3 time of best lap in the session in milliseconds   |
| uint16               | bestOverallSector1TimeInMS | // Best overall sector 1 time of the session in milliseconds      |
| uint8                | bestOverallSector1LapNum   | // Lap number best overall sector 1 time achieved on  |
| uint16               | bestOverallSector2TimeInMS | // Best overall sector 2 time of the session in milliseconds    |
| uint8                | bestOverallSector2LapNum   | // Lap number best overall sector 2 time achieved on      |
| uint16               | bestOverallSector3TimeInMS | // Best overall sector 3 time of the session in milliseconds      |
| uint8                | bestOverallSector3LapNum    | // Lap number best overall sector 3 time achieved on    |
| float                | lapDistance   | // Distance vehicle is around current lap in metres – could be negative if line hasn’t been crossed yet     |
| float                | totalDistance   | // Total distance travelled in session in metres – could be negative if line hasn’t been crossed yet    |
| float                | safetyCarDelta  | // Delta in seconds for safety car   |
| uint8                | carPosition | // Car race position    |
| uint8                | currentLapNum  | // Current lap number     |
| uint8                | pitStatus  | // 0 = none, 1 = pitting, 2 = in pit area     |
| uint8                | sector   | // 0 = sector1, 1 = sector2, 2 = sector3                                                                                  |
| uint8                | currentLapInvalid  | // Current lap invalid - 0 = valid, 1 = invalid       |
| uint8                | penalties   | // Accumulated time penalties in seconds to be added  |
| uint8                | gridPosition  | // Grid position the vehicle started the race in   |
| uint8                | driverStatus   | // Status of driver - 0 = in garage, 1 = flying lap, 2 = in lap, 3 = out lap, 4 = on track   |
| uint8                | resultStatus   | // Result status - 0 = invalid, 1 = inactive, 2 = active, 3 = finished, 4 = disqualified, 5 = not classified, 6 = retired |

# 4. TelemetryData
| Datatype                           | Variable                        | Comment                                                                                                                               |
|------------------------------------|---------------------------------|---------------------------------------------------------------------------------------------------------------------------------------|
| PacketHeader                       | m_header;                       | // Header                                                                                                                             |
| CarTelemetryData                   |     m_carTelemetryData[22];     |                                                                                                                                       |
| uint32                             |         m_buttonStatus;         | // Bit flags specifying which buttons are being pressed currently - see appendices                                                    |
| // Added in Beta 3:                |                                 |                                                                                                                                       |
| uint8                              |         m_mfdPanelIndex;        | // Index of MFD panel open - 255 = MFD closed Single player, race – 0 = Car setup, 1 = Pits, 2 = Damage, 3 = Engine, 4 = Temperatures |
| // May vary depending on game mode |                                 |                                                                                                                                       |
| uint8                              | m_mfdPanelIndexSecondaryPlayer; | // See above                                                                                                                          |
| int8                               |         m_suggestedGear;        | // Suggested gear for the player (1-8), 0 if no gear suggested                                                                        |

## 4.1 -> TelemetryData (for each car)
| Datatype | Variable                      | Comment                                                      |
|----------|-------------------------------|--------------------------------------------------------------|
| uint16   | speed                         | // Speed of car in kilometres per hour                       |
| float    | throttle                      | // Amount of throttle applied (0.0 to 1.0)                   |
| float    | steer                         | // Steering (-1.0 (full lock left) to 1.0 (full lock right)) |
| float    | brake                         | // Amount of brake applied (0.0 to 1.0)                      |
| uint8    | clutch                        | // Amount of clutch applied (0 to 100)                       |
| int8     | gear                          | // Gear selected (1-8, N=0, R=-1)                            |
| uint16   | engineRPM                     | // Engine RPM                                                |
| uint8    | drs                           | // 0 = off, 1 = on                                           |
| uint8    | revLightsPercent              | // Rev lights indicator (percentage)                         |
| uint16   | brakesTemperature[4]          | // Brakes temperature (celsius)                              |
| uint8    | tyresSurfaceTemperature[4]    | // Tyres surface temperature (celsius)                       |
| uint8    | tyresInnerTemperature[4]      | // Tyres inner temperature (celsius)                         |
| uint16   | engineTemperature             | // Engine temperature (celsius)                              |
| float    | tyresPressure[4]              | // Tyres pressure (PSI)                                      |
| uint8    | surfaceType[4]                | // Driving surface, see appendices                           |
