# 1. F1 Telemetry Data Anaylsis
Data analysis has become a major success factor in modern motorsports. Gathering and analyzing the data is one of the key capabilities of a successful racing team and has also its place in the ESports equivalents.

This repository provides the code to gather session data from the F1 2020 game and to store them in a TimeSeries database (InfluxDB).

Afterwards, an Open Source Real-Time Dashboard (Grafana) can be configured and used for session monitoring as well as analysis of the data post-race:
## 1.1 Example Race Engineer View
<img src="/img/dashboard_example1.png">

## 1.2 Example Realtime Telemetry View
<img src="/img/dashboard_example2.png">

## 1.3 Example Race Analysis
<img src="/img/race_analysis_dashboard_example.png">

## 1.4 Example Analysis of Laptime Distribution
<img src="/img/notebook_example1.png">

## 1.5 Example Comparison of two Laps
<img src="/img/notebook_example2.png">

# 2. Architecture
In order to have as low latency as possible, I would recommend to use the PC/Mac inside the Home Network. But of course, a setup in the cloud would also be possible. 

## 2.1 High Level Architecture
<img src="/architecture/architecture.jpg">

## 2.2 Time-Series Database Concept 
<img src="/architecture/influxdb_concept.png">

# 3. Getting Started

## 3.1 Install the components
As you can see in the architecture overview, we need to install and configure several of the displayed components:

1. Clone this repository
2. Create a virtual environment named 'env' ```python3 -m venv env/```
3. Enter the virtual environment ```source env/bin/activate```
4. Install the required packages using the requirements-file: ```pip install -r requirements.txt```
5. Install and configure InfluxDB: [>>> Link <<<](https://docs.influxdata.com/influxdb/v2.0/get-started/)
6. Install and configure Grafana: [>>> Link (for Mac) <<<](https://grafana.com/docs/grafana/latest/installation/mac/)
    - Configure the InfluxDB in the Grafana Dashboard: [>>> Link <<<](https://docs.influxdata.com/influxdb/v2.0/tools/grafana/)
7. Fill in the ```CREDENTIALS.py``` file
8. Setup F1 2020 in the UPD Telemetry settings
    - Insert your IP address (from the computer you want to receive the data)
    - Specify the port and make sure it is the same as in the ```CREDENTIALS.py```
9. If a SQL-database is also needed for structured data analysis in the jupyter notebook, create a ```streamdb.db``` file on the top level where also the python files are. Tables are being created automatically via the python script.

After that - everytime you play - F1 2020 will send packages to the network which your PC/Mac will listen to. The type of the packages sent varies and is described on the codemaster's website in detail: [>>> Link to packet description <<<](https://forums.codemasters.com/topic/50942-f1-2020-udp-specification/)

## 3.2 Run all components manually
Before starting the session in F1 2020, we have to start our infrastructure
1. Start the InfluxDB via terminal -> ```influxd```
2. Start the Grafana Dashboard -> ```./bin/grafana-server web```
3. Build/Adjust a Grafana Dashboard and set the refresh rate to a low number. Default minimum of refresh rate is set to 5s. If this is not enough, adjust the the config-file of Grafana:
    - navigate into the Grafana-folder
    - open the file ```./conf/defaults.ini```
    - change the respective line to ```min_refresh_interval = 1s```
4. start main.py -> ```python main.py```
5. start jupyter notebook -> ```jupyter notebook```

## 3.3 Run all components automatically (for optimization)
It is also possible to run all the components automatically by a bash script.
1. Use the file in the repository (execution_script) and adjust the parameters like 
    - directory of the Grafana installation
    - directory of the Python environment
    - browser command....
2. For the first time, we have to tell bash, that it is an executable script with ```chmod 700 execution_script```
3. Run the whole script by either double clicking or using the command ```./execution_script```

Now create your dashboards and enjoy driving while your pc will record every single action on track!
