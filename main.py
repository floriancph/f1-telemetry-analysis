from TelemetryServer import telemetry_server

if __name__ == "__main__":
    session_name = str(input("Name of your session: ")).strip().lower()
    telemetry_server.start_session(session_name)
